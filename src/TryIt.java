public class TryIt {

    public static void main(String[] args) {
        // Note that B extends A
        A x = new B();
        B y = new B();
        System.out.println("A.foo() = " + A.foo());
        System.out.println("B.foo() = " + B.foo());
        System.out.println("x.foo() = " + x.foo());
        System.out.println("y.foo() = " + y.foo());
        System.out.println("((A)x).foo() = " + ((A)x).foo());
        System.out.println("((B)x).foo() = " + ((B)x).foo());
        System.out.println("((A)y).foo() = " + ((A)y).foo());
    }
}

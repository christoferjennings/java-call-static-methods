# Call static methods

Run these commands...
```bash
javac src/*.java
java -cp src/ TryIt
```

Running `TryIt` should result in...
```bash
A.foo() = 1
B.foo() = 2
x.foo() = 1
y.foo() = 2
((A)x).foo() = 1
((B)x).foo() = 2
((A)y).foo() = 1
```

Calls to static methods uses the Type (Class) not the Instance.